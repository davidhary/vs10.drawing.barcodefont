Namespace BarcodeHarness
	Partial Public Class BarcodeTestHarness
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BarcodeTestHarness))
            Me._InputStringLabel = New System.Windows.Forms.Label()
            Me._TextToConvertTextBox = New System.Windows.Forms.TextBox()
            Me._Convert128Button = New System.Windows.Forms.Button()
            Me._EncodedTextTextBox = New System.Windows.Forms.TextBox()
            Me._EncodedStringLabel = New System.Windows.Forms.Label()
            Me._Convert39Button = New System.Windows.Forms.Button()
            Me._PrintButton = New System.Windows.Forms.Button()
            Me._BarcodeLabel = New System.Windows.Forms.Label()
            Me._PrintPreviewDialog = New System.Windows.Forms.PrintPreviewDialog()
            Me._PrintPreviewButton = New System.Windows.Forms.Button()
            Me._ChecksumInfoLabel = New System.Windows.Forms.Label()
            Me.SuspendLayout()
            '
            '_InputStringLabel
            '
            Me._InputStringLabel.AutoSize = True
            Me._InputStringLabel.Location = New System.Drawing.Point(15, 14)
            Me._InputStringLabel.Name = "_InputStringLabel"
            Me._InputStringLabel.Size = New System.Drawing.Size(92, 16)
            Me._InputStringLabel.TabIndex = 0
            Me._InputStringLabel.Text = "Text to convert"
            '
            '_TextToConvertTextBox
            '
            Me._TextToConvertTextBox.Location = New System.Drawing.Point(117, 10)
            Me._TextToConvertTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._TextToConvertTextBox.Name = "_TextToConvertTextBox"
            Me._TextToConvertTextBox.Size = New System.Drawing.Size(348, 22)
            Me._TextToConvertTextBox.TabIndex = 1
            Me._TextToConvertTextBox.Text = "3754 KC 75"
            '
            '_Convert128Button
            '
            Me._Convert128Button.Location = New System.Drawing.Point(271, 41)
            Me._Convert128Button.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._Convert128Button.Name = "_Convert128Button"
            Me._Convert128Button.Size = New System.Drawing.Size(96, 28)
            Me._Convert128Button.TabIndex = 2
            Me._Convert128Button.Text = "Convert 128"
            Me._Convert128Button.UseVisualStyleBackColor = True
            '
            '_EncodedTextTextBox
            '
            Me._EncodedTextTextBox.Location = New System.Drawing.Point(117, 76)
            Me._EncodedTextTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._EncodedTextTextBox.Name = "_EncodedTextTextBox"
            Me._EncodedTextTextBox.ReadOnly = True
            Me._EncodedTextTextBox.Size = New System.Drawing.Size(348, 22)
            Me._EncodedTextTextBox.TabIndex = 3
            '
            '_EncodedStringLabel
            '
            Me._EncodedStringLabel.AutoSize = True
            Me._EncodedStringLabel.Location = New System.Drawing.Point(15, 80)
            Me._EncodedStringLabel.Name = "_EncodedStringLabel"
            Me._EncodedStringLabel.Size = New System.Drawing.Size(79, 16)
            Me._EncodedStringLabel.TabIndex = 4
            Me._EncodedStringLabel.Text = "Return value"
            '
            '_Convert39Button
            '
            Me._Convert39Button.Location = New System.Drawing.Point(370, 41)
            Me._Convert39Button.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._Convert39Button.Name = "_Convert39Button"
            Me._Convert39Button.Size = New System.Drawing.Size(96, 28)
            Me._Convert39Button.TabIndex = 5
            Me._Convert39Button.Text = "Convert 39"
            Me._Convert39Button.UseVisualStyleBackColor = True
            '
            '_PrintButton
            '
            Me._PrintButton.Location = New System.Drawing.Point(370, 165)
            Me._PrintButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._PrintButton.Name = "_PrintButton"
            Me._PrintButton.Size = New System.Drawing.Size(96, 28)
            Me._PrintButton.TabIndex = 6
            Me._PrintButton.Text = "Print"
            Me._PrintButton.UseVisualStyleBackColor = True
            '
            '_BarcodeLabel
            '
            Me._BarcodeLabel.AutoSize = True
            Me._BarcodeLabel.BackColor = System.Drawing.Color.Transparent
            Me._BarcodeLabel.Font = New System.Drawing.Font("Code 128", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
            Me._BarcodeLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me._BarcodeLabel.Location = New System.Drawing.Point(117, 107)
            Me._BarcodeLabel.Name = "_BarcodeLabel"
            Me._BarcodeLabel.Size = New System.Drawing.Size(188, 47)
            Me._BarcodeLabel.TabIndex = 10
            Me._BarcodeLabel.Text = "Barcode Label"
            '
            '_PrintPreviewDialog
            '
            Me._PrintPreviewDialog.AutoScrollMargin = New System.Drawing.Size(0, 0)
            Me._PrintPreviewDialog.AutoScrollMinSize = New System.Drawing.Size(0, 0)
            Me._PrintPreviewDialog.ClientSize = New System.Drawing.Size(400, 300)
            Me._PrintPreviewDialog.Enabled = True
            Me._PrintPreviewDialog.Icon = CType(resources.GetObject("_PrintPreviewDialog.Icon"), System.Drawing.Icon)
            Me._PrintPreviewDialog.Name = "_PrintPreviewDialog"
            Me._PrintPreviewDialog.Visible = False
            '
            '_PrintPreviewButton
            '
            Me._PrintPreviewButton.Location = New System.Drawing.Point(221, 165)
            Me._PrintPreviewButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._PrintPreviewButton.Name = "_PrintPreviewButton"
            Me._PrintPreviewButton.Size = New System.Drawing.Size(146, 28)
            Me._PrintPreviewButton.TabIndex = 12
            Me._PrintPreviewButton.Text = "Print Preview"
            Me._PrintPreviewButton.UseVisualStyleBackColor = True
            '
            '_ChecksumInfoLabel
            '
            Me._ChecksumInfoLabel.AutoSize = True
            Me._ChecksumInfoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._ChecksumInfoLabel.Location = New System.Drawing.Point(16, 98)
            Me._ChecksumInfoLabel.Name = "_ChecksumInfoLabel"
            Me._ChecksumInfoLabel.Size = New System.Drawing.Size(76, 9)
            Me._ChecksumInfoLabel.TabIndex = 13
            Me._ChecksumInfoLabel.Text = "(Includes checksum)"
            '
            'BarcodeTestHarness
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(479, 209)
            Me.Controls.Add(Me._ChecksumInfoLabel)
            Me.Controls.Add(Me._PrintPreviewButton)
            Me.Controls.Add(Me._BarcodeLabel)
            Me.Controls.Add(Me._PrintButton)
            Me.Controls.Add(Me._Convert39Button)
            Me.Controls.Add(Me._EncodedStringLabel)
            Me.Controls.Add(Me._EncodedTextTextBox)
            Me.Controls.Add(Me._Convert128Button)
            Me.Controls.Add(Me._TextToConvertTextBox)
            Me.Controls.Add(Me._InputStringLabel)
            Me.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me.Name = "BarcodeTestHarness"
            Me.Text = "Barcode Test Harness"
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private _InputStringLabel As System.Windows.Forms.Label
        Private _TextToConvertTextBox As System.Windows.Forms.TextBox
        Private WithEvents _Convert128Button As System.Windows.Forms.Button
        Private _EncodedTextTextBox As System.Windows.Forms.TextBox
        Private _EncodedStringLabel As System.Windows.Forms.Label
        Private WithEvents _Convert39Button As System.Windows.Forms.Button
        Private WithEvents _PrintButton As System.Windows.Forms.Button
        Private _BarcodeLabel As System.Windows.Forms.Label
        Private _PrintPreviewDialog As System.Windows.Forms.PrintPreviewDialog
        Private WithEvents _PrintPreviewButton As System.Windows.Forms.Button
        Private _ChecksumInfoLabel As System.Windows.Forms.Label
    End Class
End Namespace

