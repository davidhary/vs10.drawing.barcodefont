Imports System.ComponentModel
Imports System.Drawing.Text
Imports System.Text
Imports JTBarton.Drawing.BarcodeFont
Imports System.Drawing.Printing

Namespace BarcodeHarness
	Partial Public Class BarcodeTestHarness
		Inherits Form

		Private convertType As Integer
		Private pd As New PrintDocument()

		Public Sub New()
			InitializeComponent()
		End Sub

        Private Sub btnConvert128_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _Convert128Button.Click
            _EncodedTextTextBox.Text = Barcode128.Encode(_TextToConvertTextBox.Text)
            convertType = CalculateConvertType(_TextToConvertTextBox.Text, 1)
        End Sub

        Private Sub btnConvert39_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _Convert39Button.Click
            ' Convert with checksum
            _EncodedTextTextBox.Text = Barcode39.Encode(_TextToConvertTextBox.Text, True)
            convertType = CalculateConvertType(_EncodedTextTextBox.Text, 2)
        End Sub

        Private Function CalculateConvertType(ByVal outputString As String, ByVal GoodReturnValue As Integer) As Integer
            If Not String.IsNullOrWhiteSpace(outputString) Then
                If GoodReturnValue = 1 Then
                    _BarcodeLabel.Font = New Font(Barcode128.FontName, 36)
                Else
                    _BarcodeLabel.Font = New Font(Barcode39.FontName, 36)
                End If
                _BarcodeLabel.Text = outputString
                _PrintButton.Enabled = True
                _PrintPreviewButton.Enabled = True
                Return GoodReturnValue
            End If
            _BarcodeLabel.Text = ""
            _PrintButton.Enabled = False
            _PrintPreviewButton.Enabled = False
            Return 0
        End Function

        Private Sub BarcodeTestHarness_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            convertType = CalculateConvertType("", 0)
            AddHandler pd.PrintPage, AddressOf pd_PrintPage
        End Sub


        Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _PrintButton.Click
            pd.Print()
        End Sub

        Private Sub btnPreview_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _PrintPreviewButton.Click
            _PrintPreviewDialog.Document = pd
            CType(_PrintPreviewDialog, Form).WindowState = FormWindowState.Maximized
            _PrintPreviewDialog.ShowDialog()
        End Sub


        Private Sub pd_PrintPage(ByVal sender As Object, ByVal ev As PrintPageEventArgs)
            Dim pf As New PointF(ev.MarginBounds.Left, ev.MarginBounds.Top)
            pf = PrintString(ev, pf, "Printing", "Arial", 14, False)
            If convertType = 1 Then
                pf = PrintString(ev, pf, "Code128", "Arial", 14, True)
            Else
                pf = PrintString(ev, pf, "Code 39", "Arial", 14, True)
            End If
            pf.Y += 15
            pf = PrintString(ev, pf, "Original Text : " & _TextToConvertTextBox.Text, "Arial", 10, True)
            pf = PrintString(ev, pf, "Encoded Text : " & _EncodedTextTextBox.Text, "Arial", 10, True)
            pf.Y += 10
            If convertType = 1 Then
                pf = PrintString(ev, pf, _EncodedTextTextBox.Text, Barcode128.FontName, 28, True)
            Else
                pf = PrintString(ev, pf, _EncodedTextTextBox.Text, Barcode39.FontName, 28, True)
            End If
        End Sub


        ''' <summary> Print string. </summary>
        ''' <param name="ev">      Print page event information. </param>
        ''' <param name="pf">      The pf. </param>
        ''' <param name="value">   The value. </param>
        ''' <param name="font">    The font. </param>
        ''' <param name="size">    The size. </param>
        ''' <param name="newline"> true to newline. </param>
        Private Shared Function PrintString(ByVal ev As PrintPageEventArgs, ByVal pf As PointF, ByVal value As String, ByVal font As String, ByVal size As Single, ByVal newline As Boolean) As PointF
            Using printFont As New Font(font, size)
                ev.Graphics.DrawString(value, printFont, Brushes.Black, pf)
                If Not newline Then
                    pf.X += ev.Graphics.MeasureString(value, printFont).Width
                Else
                    pf.X = ev.MarginBounds.Left
                    pf.Y += (printFont.GetHeight(ev.Graphics))
                End If
            End Using
            Return pf
        End Function
    End Class

End Namespace