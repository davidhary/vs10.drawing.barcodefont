Imports System.Text

''' <summary> Code 39 Convert an input string to the equivalent string including start and stop
''' characters. This object compresses the values to the shortest possible code 39 barcode
''' format. </summary>
''' <license> (c) 2006 JTBarton.com.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="7/2/2013" by="David" revision=""> Converted to VB.NET using source from
''' http://www.jtbarton.com/Barcodes/BarcodeOverview.aspx
''' </history>
Public NotInheritable Class Barcode39

    ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
    Private Sub New()
    End Sub

    ''' <summary> Name of the font. </summary>
    Public Shared ReadOnly FontName As String = "Code 3 de 9"

    ''' <summary>
    ''' Converts an input string to the equivalent string, that need to be produced using the 'Code 3 de 9' font.
    ''' </summary>
    ''' <param name="value">String to be encoded</param>
    ''' <returns>Encoded string start/stop characters included</returns>
    Public Shared Function Encode(ByVal value As String) As String
        Return Encode(value, False)
    End Function

    ''' <summary> Converts an input string to the equivalent string, that need to be produced using the 'Code 3
    ''' de 9' font. </summary>
    ''' <param name="value">       String to be encoded. </param>
    ''' <param name="addChecksum"> Is checksum to be added. </param>
    ''' <returns> Encoded string start/stop and checksum characters included.<para>
    ''' A string which give the bar code when it is displayed with CODE39.TTF font or an empty
    ''' string if the supplied parameter is no good.</para> </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Public Shared Function Encode(ByVal value As String, ByVal addChecksum As Boolean) As String

        Dim isValid As Boolean = True
        Dim currentChar As Char
        Dim returnValue As String = String.Empty
        Dim checksum As Integer = 0
        If Not String.IsNullOrWhiteSpace(value) Then

            'Check for valid characters
            For CharPos As Integer = 0 To value.Length - 1
                currentChar = Char.Parse(value.Substring(CharPos, 1))
                If Not ((currentChar >= "0"c AndAlso currentChar <= "9"c) OrElse
                        (currentChar >= "A"c AndAlso currentChar <= "Z"c) OrElse
                        currentChar = " "c OrElse currentChar = "-"c OrElse
                        currentChar = "."c OrElse currentChar = "$"c OrElse
                        currentChar = "/"c OrElse currentChar = "+"c OrElse
                        currentChar = "%"c) Then
                    isValid = False
                    Exit For
                End If
            Next CharPos
            If isValid Then
                ' Add start char
                returnValue = "*"
                ' Add other chars, and calc checksum
                For CharPos As Integer = 0 To value.Length - 1
                    currentChar = Char.Parse(value.Substring(CharPos, 1))
                    returnValue &= currentChar.ToString()
                    If currentChar >= "0"c AndAlso currentChar <= "9"c Then
                        checksum = checksum + AscW(currentChar) - 48
                    ElseIf currentChar >= "A"c AndAlso currentChar <= "Z"c Then
                        checksum = checksum + AscW(currentChar) - 55
                    Else
                        Select Case currentChar
                            Case "-"c
                                checksum = checksum + AscW(currentChar) - 9
                            Case "."c
                                checksum = checksum + AscW(currentChar) - 9
                            Case "$"c
                                checksum = checksum + AscW(currentChar) + 3
                            Case "/"c
                                checksum = checksum + AscW(currentChar) - 7
                            Case "+"c
                                checksum = checksum + AscW(currentChar) - 2
                            Case "%"c
                                checksum = checksum + AscW(currentChar) + 5
                            Case " "c
                                checksum = checksum + AscW(currentChar) + 6
                        End Select
                    End If
                Next CharPos
                ' Calculation of the checksum ASCII code
                If addChecksum Then
                    checksum = checksum Mod 43
                    If checksum >= 0 AndAlso checksum <= 9 Then
                        returnValue &= (ChrW(checksum + 48)).ToString()
                    ElseIf checksum >= 10 AndAlso checksum <= 35 Then
                        returnValue &= (ChrW(checksum + 55)).ToString()
                    Else
                        Select Case checksum
                            Case 36
                                returnValue &= "-"
                            Case 37
                                returnValue &= "."
                            Case 38
                                returnValue &= " "
                            Case 39
                                returnValue &= "$"
                            Case 40
                                returnValue &= "/"
                            Case 41
                                returnValue &= "+"
                            Case 42
                                returnValue &= "%"
                        End Select
                    End If
                End If
                ' Add stop char
                returnValue &= "*"
            End If
        End If
        Return returnValue
    End Function

End Class
