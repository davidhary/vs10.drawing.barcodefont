Imports System.Text

Partial Public NotInheritable Class Barcode128

    ''' <summary> Is number. </summary>
    ''' <param name="value"> The input value. </param>
    ''' <param name="position">    The character position. </param>
    ''' <param name="minimumPosition"> The minimum character position. </param>
    ''' <returns> the minimum position. </returns>
    Private Shared Function isNumber(ByVal value As String, ByVal position As Integer, ByVal minimumPosition As Integer) As Integer

        ' if the MinCharPos characters from CharPos are numeric, then MinCharPos = -1
        minimumPosition -= 1
        If Not String.IsNullOrWhiteSpace(value) AndAlso position + minimumPosition < value.Length Then
            Do While minimumPosition >= 0
                If AscW(Char.Parse(value.Substring(position + minimumPosition, 1))) < 48 OrElse
                    AscW(Char.Parse(value.Substring(position + minimumPosition, 1))) > 57 Then
                    Exit Do
                End If
                minimumPosition -= 1
            Loop
        End If
        Return minimumPosition
    End Function

End Class
