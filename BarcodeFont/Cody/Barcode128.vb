''' <summary> Code 128 Convert an input string to the equivalent string including start and stop
''' characters. This object compresses the values to the shortest possible code 128 barcode
''' format. </summary>
''' <license> (c) 2006 JTBarton.com.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="2006" by="Cody" revision=""> Converted to VB.NET and modified for new barcode 128 font. </history>
Public NotInheritable Class Barcode128

    ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
    Private Sub New()
    End Sub

    ''' <summary> Name of the font. </summary>
    Public Shared ReadOnly FontName As String = "Code 128"

    ''' <summary> Generates an image. </summary>
    ''' <param name="value"> The barcode value. </param>
    ''' <returns> The image. </returns>
    Public Shared Function GenerateImage(ByVal value As String) As Byte()
        Return Barcode.GenerateImage(Barcode128.FontName, Barcode128.Encode(value))
    End Function

    ''' <summary> Converts an input string to the equivalent string, that need to be produced using the
    ''' 'Code 128' font. </summary>
    ''' <param name="value"> String to be encoded. </param>
    ''' <returns> Encoded string start/stop and checksum characters included.<para>
    ''' A string which give the bar code when it is displayed with CODE128.TTF font or an empty
    ''' string if the supplied parameter is no good.</para> </returns>
    Public Shared Function Encode(ByVal value As String) As String

        Dim charPos As Integer, minCharPos As Integer
        Dim currentChar As Integer, checksum As Integer
        Dim isTableB As Boolean = True, isValid As Boolean = True
        Dim returnValue As String = String.Empty

        If Not String.IsNullOrWhiteSpace(value) Then

            ' Check for valid characters
            For charCount As Integer = 0 To value.Length - 1
                'currentChar = char.GetNumericValue(value, charPos);
                currentChar = AscW(Char.Parse(value.Substring(charCount, 1)))
                If Not (currentChar >= 32 AndAlso currentChar <= 126) Then
                    isValid = False
                    Exit For
                End If
            Next

            ' Barcode is full of Ascii characters, we can now process it
            If isValid Then
                charPos = 0
                While charPos < value.Length
                    If isTableB Then
                        ' See if interesting to switch to table C
                        ' yes for 4 digits at start or end, else if 6 digits
                        If charPos = 0 OrElse charPos + 4 = value.Length Then
                            minCharPos = 4
                        Else
                            minCharPos = 6
                        End If


                        minCharPos = isNumber(value, charPos, minCharPos)

                        If minCharPos < 0 Then
                            ' Choice table C
                            If charPos = 0 Then
                                ' Starting with table C
                                ' char.ConvertFromUtf32(210);
                                returnValue = (ChrW(210)).ToString()
                            Else
                                ' Switch to table C
                                returnValue = returnValue & (ChrW(204)).ToString()
                            End If
                            isTableB = False
                        Else
                            If charPos = 0 Then
                                ' Starting with table B
                                ' char.ConvertFromUtf32(209);
                                returnValue = (ChrW(209)).ToString()

                            End If
                        End If
                    End If

                    If Not isTableB Then
                        ' We are on table C, try to process 2 digits
                        minCharPos = 2
                        minCharPos = isNumber(value, charPos, minCharPos)
                        If minCharPos < 0 Then
                            ' OK for 2 digits, process it
                            currentChar = Integer.Parse(value.Substring(charPos, 2))
                            currentChar = If(currentChar < 95, currentChar + 32, currentChar + 105) ''
                            returnValue = returnValue & (ChrW(currentChar)).ToString()
                            charPos += 2
                        Else
                            ' We haven't 2 digits, switch to table B
                            returnValue = returnValue & (ChrW(205)).ToString()
                            isTableB = True
                        End If
                    End If
                    If isTableB Then
                        ' Process 1 digit with table B
                        returnValue = returnValue & value.Substring(charPos, 1)
                        charPos += 1
                    End If
                End While

                ' Calculation of the checksum
                checksum = 0
                For [loop] As Integer = 0 To returnValue.Length - 1
                    currentChar = AscW(Char.Parse(returnValue.Substring([loop], 1)))
                    currentChar = If(currentChar < 127, currentChar - 32, currentChar - 105)
                    If [loop] = 0 Then
                        checksum = currentChar
                    Else
                        checksum = (checksum + ([loop] * currentChar)) Mod 103
                    End If
                Next

                ' Calculation of the checksum ASCII code
                checksum = If(checksum < 95, checksum + 32, checksum + 105)
                ' Add the checksum and the STOP
                returnValue = returnValue & (ChrW(checksum)).ToString() & (ChrW(211)).ToString()
            End If
        End If

        Return returnValue
    End Function

End Class
