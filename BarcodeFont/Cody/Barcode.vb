
''' <summary> Support functions for barcode encoding. </summary>
''' <license> (c) 2006 Zen Interactions. All rights reserved.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Partial Public NotInheritable Class Barcode

    ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
    Private Sub New()
    End Sub

    ''' <summary> Generates an image. </summary>
    ''' <param name="fontName">   Name of the font. </param>
    ''' <param name="value"> The barcode value. </param>
    ''' <returns> The image. </returns>
    Public Shared Function GenerateImage(ByVal fontName As String, ByVal value As String) As Byte()

        Dim barcodeSize As System.Drawing.SizeF
        Using ms As System.IO.MemoryStream = New System.IO.MemoryStream()
            Using fontFamily As System.Drawing.FontFamily = New System.Drawing.FontFamily(fontName)
                Using font As New System.Drawing.Font(fontFamily, 36)
                    Using tmpBitmap As New System.Drawing.Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
                        Using oGraphics As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(tmpBitmap)
                            oGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixel
                            barcodeSize = oGraphics.MeasureString(value, font)
                        End Using
                    End Using

                    Using newBitmap As New System.Drawing.Bitmap(CInt(barcodeSize.Width), CInt(barcodeSize.Height),
                                                                 System.Drawing.Imaging.PixelFormat.Format32bppArgb)
                        Using oGraphics As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(newBitmap)
                            oGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixel
                            Using oSolidBrushWhite As New System.Drawing.SolidBrush(System.Drawing.Color.White)
                                Using oSolidBrushBlack As New System.Drawing.SolidBrush(System.Drawing.Color.Black)
                                    oGraphics.FillRectangle(oSolidBrushWhite, New System.Drawing.Rectangle(0, 0, CInt(barcodeSize.Width),
                                                                                                           CInt(barcodeSize.Height)))
                                    oGraphics.DrawString(value, font, oSolidBrushBlack, 0, 0)
                                End Using
                            End Using
                        End Using
                        newBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)
                    End Using
                End Using
                Return ms.ToArray()
            End Using
        End Using

    End Function

End Class
