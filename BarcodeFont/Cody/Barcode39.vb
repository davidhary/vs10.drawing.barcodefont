Partial Public NotInheritable Class Barcode39

    ''' <summary> Generates an image. </summary>
    ''' <param name="value"> The barcode value. </param>
    ''' <returns> The image. </returns>
    Public Shared Function GenerateImage(ByVal value As String) As Byte()
        Return Barcode.GenerateImage(Barcode39.FontName, Barcode39.Encode(value))
    End Function

End Class
