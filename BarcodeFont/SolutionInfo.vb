﻿Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.
<Assembly: AssemblyCompany("JTBarton.com")> 
<Assembly: AssemblyCopyright("(c) 2006 JTBarton.com. All rights reserved.")> 
<Assembly: AssemblyTrademark("Licensed under The MIT License.")>
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)> 
#If DEBUG Then
<Assembly: AssemblyConfiguration("Debug")> 
#Else
<Assembly: AssemblyConfiguration("Release")> 
#End If

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Revision and Build Numbers 
' by using the '*' as shown below:
<Assembly: AssemblyVersion("1.1.*")> 
