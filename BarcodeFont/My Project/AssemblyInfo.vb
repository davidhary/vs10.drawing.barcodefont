﻿Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.
<Assembly: AssemblyTitle("Barcode Font")> 
<Assembly: AssemblyDescription("Barcode Font String Builder and Checksum Generator")>
<Assembly: AssemblyProduct("Drawing.BarcodeFont.2012")> 
<Assembly: CLSCompliant(True)> 

' Setting ComVisible to false makes the types in this assembly not visible 
' to COM components.  If you need to access a type in this assembly from 
' COM, set the ComVisible attribute to true on that type.
<Assembly: ComVisible(False)>
